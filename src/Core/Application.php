<?php

namespace Core;

/**
 * Class Application
 * @package Controller
 *
 * @author Tanateros
 */
class Application {
    static public $config;

    function __construct($config){
        session_start();
        self::$config = $config;
    }

    /**
     * @param $route
     * @param array $data
     * @return mixed
     */
    function get($route, $data = []){
        if(!$route) $route = 'default';
        if(!isset(self::$config['routes'][$route])){
            header("HTTP/1.0 404 Not Found");
            header("Status: 404 Not Found");
            echo 'Error 404';
            exit;
        }
        $class = "\\Module\\" . ucfirst(self::$config['routes'][$route]['module']) . "\\Controller\\" . ucfirst(self::$config['routes'][$route]['controller']);
        $method = self::$config['routes'][$route]['method'];
        return (new $class())->$method($data);
    }
}