<?php
/**
 * Created by PhpStorm.
 * User: Tanateros
 * Date: 13.01.2016
 * Time: 19:06
 */

namespace Core;


class ViewTemplate
{
    /**
     * @param $view
     * @param array $data
     */
    static function show($view, $data = [])
    {
        $config = Application::$config;
        require $config[pathViews] . 'header' . $config['viewSufix'];
        require $config[pathViews] . $view . $config['viewSufix'];
        require $config[pathViews] . 'footer' . $config['viewSufix'];
    }
}