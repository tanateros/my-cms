<?php
/**
 * Created by PhpStorm.
 * User: Tanateros
 * Date: 13.01.2016
 * Time: 20:34
 */

namespace Module\Comment\Model;

use Core\Database;

class Comment
{
    private $db;

    function __construct()
    {
        $this->db = Database::getInstance()->db;
        return $this;
    }

    /**
     * @param string $order
     * @return array
     */
    function showAll($order = 'id') // учитывая что auto increment на id - вместо добавления доп.поля create_datetime воспользуемся им
    {
        $stmt = $this->db->query("SELECT * FROM comments ORDER BY $order ASC");
        return $stmt->fetchAll();
    }

    /**
     * @param int $id
     * @return array
     */
    function getId($id)
    {
        $stmt = $this->db->query("SELECT * FROM comments WHERE id=$id");
        return $stmt->fetchAll();
    }

    /**
     * @param int $id
     * @param array $post
     * @return $this
     */
    function edit($id, $post)
    {
        $qry = $this->db->prepare('UPDATE comments SET name=?, email=?, text=? WHERE id='.$id);
        $qry->execute(array($post['name'], $post['email'], $post['text']));
        return $this;
    }
}