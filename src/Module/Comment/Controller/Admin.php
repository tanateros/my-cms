<?php

namespace Module\Comment\Controller;

use Module\Comment\Model\Comment as CommentModel;
use Core\ViewTemplate as View;

/**
 * Class Admin
 * @package Controller
 */
class Admin
{

    /**
     * @return $this
     */
    function login()
    {
        if ($_POST['login'] == 'admin' && $_POST['password'] == '123') {
            $_SESSION['isAdmin'] = true;
            header("Location: ?route=admin");
        } else
            echo View::show('..\Module\Comment\View\login');
        return $this;
    }
/*
    function admin()
    {
        if ($_SESSION['isAdmin']) {
            $modelComment = new CommentModel();
            $data = $modelComment->showAll(!isset($_GET['order']) ?: $_GET['order']);
            echo View::show('..\Module\Comment\View\admin-comments', $data);
        } else {
            header("Location: ?route=login");
        }
    }

    function adminCommentEdit()
    {
        if ($_SESSION['isAdmin']) {
            $modelComment = new CommentModel();
            $id = (int) $_GET['id'];
            if (!empty($_POST)) {
                $modelComment->edit($id, $_POST);
            }
            $data = $modelComment->getId($id);
            echo View::show('..\Module\Comment\View\new-comment', $data);
        } else {
            header("Location: ?route=login");
        }
    }
*/
    function logout()
    {
        session_destroy();
        header("Location: ?route=login");
    }
}