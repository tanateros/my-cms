<?php

namespace Module\Comment\Controller;

use Core\ViewTemplate as View;
use Core\Manager as EntityManager;
use Module\Comment\Entity\comments as CommentModel;

/**
 * Class Comment
 * @package Module\Comment\Controller
 */
class Comment
{
    function index()
    {
        $em = (new EntityManager($GLOBALS["config"]['db']))->getManager();

        // $res = $em->getRepository('Module\Comment\Entity\comments')->findBy(array('id' => 1));
        $data = $em->getRepository('Module\Comment\Entity\comments')->findAll();
        echo View::show('..\Module\Comment\View\new-comment', $data);
    }

    function add()
    {
        header('Refresh: 1; url=/');
        $args = array(
            'name'   => FILTER_SANITIZE_SPECIAL_CHARS,
            'email'  => FILTER_VALIDATE_EMAIL,
            'text'   => array(
                'filter'    => FILTER_SANITIZE_SPECIAL_CHARS,
                'options'   => array('min_range' => 1, 'max_range' => 5000)
            ),
        );
        $data = filter_input_array(INPUT_POST, $args);

        $comment = new CommentModel();
        $comment->setName($data['name']);
        $comment->setEmail($data['email']);
        $comment->setText($data['text']);

        $em = (new EntityManager($GLOBALS["config"]['db']))->getManager();
        $em->persist($comment);
        $em->flush();

        echo 'Вы успешно добавили запись. Через 1 секунду будете перенаправлены обратно на страницу.';
    }
}