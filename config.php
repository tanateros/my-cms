<?php

$config['pathViews'] = __DIR__ . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'View' . DIRECTORY_SEPARATOR;
$config['viewSufix'] = '.phtml';
$config['rootPath'] = __DIR__;
$config['routes'] = [
    'default' => ['module' => 'comment', 'controller' => 'comment', 'method' => 'index'],
    /**
     * 'error404' => ['controller'=>'ErrorPage', 'method'=>'err404'],
     * 'error500' => ['controller'=>'ErrorPage', 'method'=>'err500'],
     */

    'edit' => ['module' => 'comment', 'controller' => 'admin', 'method' => 'adminCommentEdit'],
    'admin' => ['module' => 'comment', 'controller' => 'admin', 'method' => 'admin'],
    'add' => ['module' => 'comment', 'controller' => 'comment', 'method' => 'add'],
    'login' => ['module' => 'comment', 'controller' => 'admin', 'method' => 'login'],
    'logout' => ['module' => 'comment', 'controller' => 'admin', 'method' => 'logout'],
];
$config['db'] = [
    'driver' => 'pdo_sqlite',
    'path' => __DIR__ . DIRECTORY_SEPARATOR . 'databaseSQLite.db',
    /**
     *           'user' => 'root',
     *           'password' => '123',
     *           'dbname' => 'db',
     */
];
$config['module'] = [
    'Comment',
    'Dashboard',
];
