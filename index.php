<?php

require 'bootstrap.php';

if(file_exists('install.php')) {
    if($_GET['route'] == 'delete_install_file') {
        unlink('install.php');
        header("Refresh: 1; /");
    }
    else {
        header("Location: /install.php");
    }
}
$app = new Core\Application($config);

$app->get($_GET['route']);

/*
 * // install.php
<?php

require 'bootstrap.php';
try {
    header("Refresh: 5; /?route=delete_install_file");
    Core\InstallDumpSqlite::getInstance();
    echo 'Установка прошла успешно. Вы будете переведены на главную страницу через 5 секунд.';
} catch (Exception $e) {
    echo 'Ошибка установки: ' . $e;
}

*/