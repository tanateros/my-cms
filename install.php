<?php

require 'bootstrap.php';
try {
    header("Refresh: 5; /?route=delete_install_file");
    Core\InstallDumpSqlite::getInstance();
    echo 'Установка прошла успешно. Вы будете переведены на главную страницу через 5 секунд.';
} catch (Exception $e) {
    echo 'Ошибка установки: ' . $e;
}
